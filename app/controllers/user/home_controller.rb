class User::HomeController < User::ApplicationController

  def index
    @projects = Project.all
    mineria = 0
    energia = 0
    infraestructura = 0
    forestal = 0
    industrial = 0
    sanitario = 0
    todos = ProjectTemp.all.count
    ProjectTemp.all.each do |project|
      if project.sector_name == "Minería"
        mineria = mineria + 1
      end
       if project.sector_name == "Forestal"
        forestal = forestal + 1
      end
      if project.sector_name == "Infraestructura"
        infraestructura = infraestructura + 1
      end
      if project.sector_name == "Energía"
        energia = energia + 1
      end
      if project.sector_name == "Industrial"
        industrial = industrial + 1
      end
      if project.sector_name == "Sanitario"
        sanitario = sanitario + 1
      end
    end
  end

end
